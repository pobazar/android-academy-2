package com.example.androidacademy;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity
{
    public static final String KEY_Message = "KEY_Message";
    private TextView textMes;
    private Button butEm;
    public static final String LOG="My_Log";
    public String message;
    public String addresses="Android@gmail.com";
    public String subject="First exercise";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Log.d(LOG,"Second activity is open");

        message = getIntent().getStringExtra(KEY_Message);

        butEm=findViewById((R.id.button_email));

        textMes = findViewById(R.id.text_message);
        textMes.setText(message);

        butEm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d(LOG,"Try open mail");
                openEmailActivity();
            }
        });
    }
    public void openEmailActivity()
    {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse(getString(R.string.email_adr, addresses)));

        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        if (intent.resolveActivity(getPackageManager()) != null)
        {
            startActivity(intent);
        }
        else
        {
            Toast.makeText(this, "No Email app found", Toast.LENGTH_LONG).show();
        }
    }
}
