package com.example.androidacademy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    private Button butPrv;
    private TextView editMes;
    private String message;
    public static final String KEY_Message = "KEY_Message";
    public static final String LOG="My_Log";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editMes=findViewById(R.id.edit_message);
        butPrv=findViewById(R.id.button_preview);


        butPrv.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d(LOG,"Try open second activity");
                message=editMes.getText().toString();
                openSecondActivity();
            }
        });
    }

    public void openSecondActivity()
    {
        Intent secondActivityIntent = new Intent(this, SecondActivity.class);
        secondActivityIntent.putExtra(KEY_Message, message);
        startActivity(secondActivityIntent);
    }
}
